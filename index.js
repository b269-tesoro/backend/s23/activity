// initialization
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	talk: function(index){
		console.log(this.pokemon[index] + "! I choose you!");
	}
};

console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk method:");
console.log(trainer.talk(0));





// constructor
function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        let remainingHealth = target.health - this.attack;
        
        console.log(target.name + "'s health is now reduced to " + remainingHealth);
        target.health = remainingHealth;

        if(remainingHealth <=0){
        	target.faint()
        	} 
        };
    this.faint = function(){
        console.log(this.name + ' fainted.');
    }

}

let pikachu = new Pokemon("Pikachu", 30);
let dragonite = new Pokemon("Dragonite", 100);
let charizard = new Pokemon("Charizard", 50);

console.log(pikachu);
console.log(dragonite);
console.log(charizard);

// combat simulation
dragonite.tackle(pikachu);
console.log(pikachu);

dragonite.tackle(charizard);
console.log(charizard);


